# Game Boy Advance

Shows two rectangles that bounce around on screen and change color on collision. Can be opened in GameBoy Advance emulator. 

# Images
![GBA](https://gitlab.com/diveshj/project-images-dump/-/raw/main/GBA-BoxCollision/GBA.jpg)
