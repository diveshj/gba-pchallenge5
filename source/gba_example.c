
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;

#define REG_DISPLAYCONTROL  *((volatile uint32*)(0x04000000))
#define VIDEO_MODE_3        0x0003
#define BGMODE_2            0x0400

#define REG_VCOUNT          (*(volatile uint16*)0x04000006)

#define SCREENBUFFER        ((volatile uint16*)0x06000000)
#define SCREEN_W            240
#define SCREEN_H            160

// 0000 0000 0000 0000
// r = 1111 1111
// & = 0001 1111
// g = 0000 0000
// b = 0000 0000
// c = 0 00000 00111 11111

inline uint16 Color(uint8 red, uint8 green, uint8 blue)
{
    return (red & 0x1F) | (green & 0x1F) << 5 | (blue & 0x1F) << 10;
}

inline int Clamp(int value, int min, int max)
{
    return (value < min ? min : (value > max ? max : value));
}
struct rect {
unsigned short x, y, w, h, xfac, yfac, color;
};
inline int abs(int value)
{
    int mask = value >> 31;
    return (value ^ mask) - mask;
}

inline void vsync()
{
    while(REG_VCOUNT >= 160);
    while(REG_VCOUNT < 160);
}

inline void draw_rect(struct rect* s)
{
        for(int y = s->y; y < s->h; y++)
        {
            for(int x = s->x; x < s->w; x++)
            {
                SCREENBUFFER[(y * SCREEN_W) + x ] = s->color;
            }
        }
}

inline void move_rect(struct rect* s)
{
    s->x += s->xfac;
    s->y += s->yfac;
    s->w += s->xfac;
    s->h += s->yfac;
}

inline void clear_screen(struct rect* s)
{
    int x, y;
    for(y = s->y; y <s->h; y++)
    {
        for(x = s->x; x <s->w; x++)
        {
                SCREENBUFFER[(y * SCREEN_W) + x ] = 0;
        }
    }
}

int main(void)
{
    REG_DISPLAYCONTROL = VIDEO_MODE_3 | BGMODE_2;

    struct rect s = {2, 2, 10 , 12, 1, 1, Color(255, 0, 0)};
    struct rect s2 = {50, 50, 60 , 72, -1,-1, Color(0, 255, 0)};
    while(1)
    {
        vsync();
        clear_screen(&s);
        clear_screen(&s2);
        move_rect(&s);
        move_rect(&s2);

        #pragma region sq1 collision with edge

        if(s.x <= 0)
        {
            s.xfac = -s.xfac;
            s.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        if(s.y <= 0)
        {
            s.yfac = -s.yfac;
            s.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        if(s.w > SCREEN_W)
        {
            s.xfac = -s.xfac;
            s.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        if(s.h > SCREEN_H)
        {
            s.yfac = -s.yfac;
            s.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        #pragma endregion
        #pragma region sq2 collision with edge
        if(s2.x <= 0)
        {
            s2.xfac = -s2.xfac;
            s2.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        if(s2.y <= 0)
        {
            s2.yfac = -s2.yfac;
            s2.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        if(s2.w > SCREEN_W)
        {
            s2.xfac = -s2.xfac;
            s2.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        if(s2.h > SCREEN_H)
        {
            s2.yfac = -s2.yfac;
            s2.color = Color(rand() % 255, rand() % 255, rand() % 255);
        }
        #pragma endregion
        #pragma region sq1 collision with sq2
        if(s.x < s2.w && s.y < s2.h && s.w > s2.x && s.h > s2.y)
        {
            s.xfac = -s.xfac;
            s.yfac = -s.yfac;
            s2.xfac = -s2.xfac;
            s2.yfac = -s2.yfac;
        }
        
        #pragma endregion
        draw_rect(&s);
        draw_rect(&s2);
    }
    return 0;
}
